extern crate macrodcl;

use macrodcl::error::MDCLError;
use macrodcl::macros::process_macros;
use macrodcl::output::DCLOutputtable;
use macrodcl::parser::parse_program;
use macrodcl::passes::*;
use macrodcl::tokenizer::tokenize;
use macrodcl::tokenizer::Token;

use std::cmp::Ordering;
use std::env::args_os;
use std::fs::File;
use std::io::prelude::*;
use std::io::stdin;

fn compile_and_print<'a>(program: &'a str) -> Result<(), MDCLError<'a>> {
    let parsed_program = parse_program(process_macros(tokenize(program)?.0)?)?;
    let unblocked = variable_pass(&unblock_whens(&fold_constants(&parsed_program))?)?;
    let mut split = split_sinks(&unblocked)?;
    let mut subprograms: Vec<_> = split.drain().collect();
    subprograms.sort_unstable_by(|a, b| match (&a.0, &b.0) {
        (None, None) => Ordering::Equal,
        (None, Some(_)) => Ordering::Less,
        (Some(_), None) => Ordering::Greater,
        (Some(a), Some(b)) => a.contents().cmp(b.contents()),
    });
    println!("<meta charset='utf-8'>");
    for subprogram in subprograms {
        println!(
            "<h2>{}</h2><textarea>",
            match subprogram.0 {
                None => String::from("Top-Level"),
                Some(token) => String::from(token.contents()),
            }
        );
        println!();
        let subprogram = minify_variable_names(&variable_pass(&subprogram.1)?)?;
        let mut token_iter = subprogram
            .into_iter()
            .flat_map(|command| command.to_dcl_tokens().unwrap().into_iter())
            .peekable();
        while let Some(token) = token_iter.next() {
            print!("{}", token.contents());
            match (token, token_iter.peek()) {
                | (Token::Identifier(_), Some(Token::NumberLiteral(_)))
                | (Token::Keyword(_), Some(Token::NumberLiteral(_)))
                | (Token::NumberLiteral(_), Some(Token::NumberLiteral(_)))
                | (Token::Keyword(_), Some(Token::Keyword(_)))
                | (Token::Keyword(_), Some(Token::Identifier(_)))
                | (Token::Keyword(_), Some(Token::FunctionName(_)))
                | (Token::Identifier(_), Some(Token::Keyword(_)))
                | (Token::Identifier(_), Some(Token::Identifier(_)))
                | (Token::Identifier(_), Some(Token::FunctionName(_))) => print!("\n"),
                _ => {}
            }
        }
        println!("</textarea>\n\n");
    }
    Ok(())
}

pub fn main() -> std::io::Result<()> {
    let mut source_code = String::new();
    if args_os().count() >= 2 {
        for i in 1..args_os().count() {
            File::open(args_os().nth(i).unwrap())?.read_to_string(&mut source_code)?;
            source_code.push('\n');
        }
    } else {
        stdin().read_to_string(&mut source_code)?;
    }
    match compile_and_print(source_code.as_ref()) {
        Ok(_) => Ok(()),
        Err(error) => {
            // TODO: position information
            println!("{:?} {}", error.bad_tokens, error.description);
            Ok(())
        }
    }
}
