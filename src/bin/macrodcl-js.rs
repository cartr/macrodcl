extern crate macrodcl;

use macrodcl::error::MDCLError;
use macrodcl::macros::process_macros;
use macrodcl::parser::parse_program;
use macrodcl::parser::Expression;
use macrodcl::parser::Statement;
use macrodcl::parser::StringLiteralComponent;
use macrodcl::passes::*;
use macrodcl::tokenizer::tokenize;

use std::env::args_os;
use std::fs::File;
use std::io::prelude::*;
use std::io::stdin;
use std::str;

fn print_expression<'a, 'b>(expr: &'a Expression<'b>) {
    match expr {
        Expression::StringLiteral(components) => {
            print!("\"");
            for component in components {
                match component {
                    StringLiteralComponent::StringData(tok) => {
                        print!("{}", str::replace(tok.contents(), "\n", "\\n"))
                    }
                    StringLiteralComponent::Interpolation(interp_expr) => {
                        print!("\"+");
                        print_expression(&interp_expr);
                        print!("+\"");
                    }
                }
            }
            print!("\"");
        }
        Expression::Literal(tok) => match tok.contents() {
            "TRUE" => print!("true"),
            "FALSE" => print!("false"),
            _ => print!("{}", tok.contents()),
        },
        Expression::FunctionEvaluation { name, args } => {
            print!("func${}(", name.contents());
            for arg in args {
                print_expression(&arg);
                print!(",");
            }
            print!(")");
        }
        Expression::Variable(name) => print!("var${}", name.contents()),
        Expression::Source {
            component,
            source,
            args,
        } => {
            print!("source${}${}(", component.contents(), source.contents());
            if let Some(args) = args {
                for arg in args {
                    print_expression(&arg);
                    print!(",");
                }
            }
            print!(")");
        }
        Expression::WhenExpression { arms, otherwise } => {
            print!("(");
            for arm in arms {
                print_expression(&arm.0);
                print!("?");
                print_expression(&arm.1);
                print!(":");
            }
            print_expression(otherwise);
            print!(")");
        }
        Expression::Operation {
            lhs,
            operation,
            rhs,
        } => {
            if operation.contents() == "**" {
                print!("Math.pow(");
                print_expression(&lhs);
                print!(",");
                print_expression(&rhs);
            } else {
                print!("(");
                print_expression(&lhs);
                match operation.contents() {
                    "AND" | "and" => print!("&&"),
                    "OR" | "or" => print!("||"),
                    "=" => print!("=="),
                    "!=" => print!("!=="),
                    _ => print!("{}", operation.contents()),
                }
                print_expression(&rhs);
            }
            print!(")");
        }
        _ => eprintln!("Unknown expression while printing JavaScript"),
    }
}

fn compile_and_print<'a>(program: &'a str) -> Result<(), MDCLError<'a>> {
    let parsed_program = parse_program(process_macros(tokenize(program)?.0)?)?;
    let unblocked = variable_pass(&unblock_whens(&fold_constants(&parsed_program))?)?;
    println!("function update_mdcl_state() {{");
    // NOTE: variable_pass orders the variables correctly for this.
    for statement in unblocked {
        print!("    ");
        match statement {
            Statement::VariableAssignment { name, value } => {
                print!("var var${} = ", name.contents());
                print_expression(&value);
            }
            Statement::SinkAssignment { sink, value } => {
                print!("sink$");
                if let Some(tok) = sink.component {
                    print!("{}$", tok.contents());
                }
                print!("{}(", sink.sink.contents());
                if let Some(args) = sink.args {
                    for arg in args {
                        print_expression(&arg);
                        print!(",");
                    }
                }
                print_expression(&value);
                print!(")");
            }
            _ => eprintln!("Unknown statement while printing JavaScript"),
        }
        println!(";");
    }
    println!("}}");
    Ok(())
}

pub fn main() -> std::io::Result<()> {
    let mut source_code = String::new();
    if args_os().count() >= 2 {
        for i in 1..args_os().count() {
            File::open(args_os().nth(i).unwrap())?.read_to_string(&mut source_code)?;
            source_code.push('\n');
        }
    } else {
        stdin().read_to_string(&mut source_code)?;
    }
    match compile_and_print(source_code.as_ref()) {
        Ok(_) => Ok(()),
        Err(error) => {
            // TODO: position information
            println!("{:?} {}", error.bad_tokens, error.description);
            Ok(())
        }
    }
}
