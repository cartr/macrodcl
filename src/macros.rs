use error::HasTokens;
use error::MDCLError;
use tokenizer::Token;

use std::borrow::Cow;
use std::collections::HashMap;
use std::collections::VecDeque;

struct UserMacro<'a> {
    args: HashMap<Token<'a>, usize>,
    result: Vec<Token<'a>>,
}

#[derive(Default)]
struct MacroState<'a> {
    counters: HashMap<Token<'a>, u32>,
    macro_limit: u32,
    macros_seen: u32,
    user_macros: HashMap<Token<'a>, UserMacro<'a>>,
}

impl<'a> MacroState<'a> {
    fn do_macro(
        &mut self,
        name: Token<'a>,
        body: VecDeque<Token<'a>>,
    ) -> Result<VecDeque<Token<'a>>, MDCLError<'a>> {
        let mut body = body;
        match name.contents().as_ref() {
            "stringify" => {
                let mut result = VecDeque::new();
                result.push_back(Token::StringStart("\""));
                for token in body {
                    result.push_back(Token::StringData(Cow::from(String::from(token.contents()))));
                }
                result.push_back(Token::StringEnd("\""));
                Ok(result)
            }
            "concat_idents" => {
                let mut ident = String::new();
                for token in body {
                    ident.push_str(token.contents());
                }
                Ok(VecDeque::from(vec![Token::Identifier(Cow::from(ident))]))
            }
            "macro" => {
                if let Some(user_macro_name) = body.pop_front() {
                    let opening_bracket = body.pop_front();
                    if opening_bracket != Some(Token::Punctuation("[")) {
                        return Err(opening_bracket
                            .unwrap_or(name)
                            .make_error("Expected macro argument list"));
                    }
                    let mut user_macro_args = vec![];
                    while body.front() != Some(&Token::Punctuation("]")) {
                        if let Some(arg) = body.pop_front() {
                            user_macro_args.push(arg)
                        } else {
                            return Err(name.make_error("Unexpected end of macro argument list"));
                        }
                        let next_token = body.pop_front();
                        match next_token {
                            Some(Token::Punctuation(",")) => {},
                            Some(Token::Punctuation("]")) => break,
                            Some(tok) => return Err(tok.make_error("Expected comma or closing bracket while parsing macro argument list")),
                            None => return Err(name.make_error("Unexpected end of macro argument list"))
                        }
                    }
                    let mut args_map = HashMap::default();
                    for (i, arg) in user_macro_args.into_iter().enumerate() {
                        args_map.insert(arg, i);
                    }
                    self.user_macros.insert(
                        user_macro_name,
                        UserMacro {
                            args: args_map,
                            result: Vec::from(body),
                        },
                    );
                    Ok(VecDeque::new())
                } else {
                    Err(name.make_error("Macro definition needs to provide a name"))
                }
            }
            "set_macro_limit" => {
                let limit_token = body.pop_front();
                if let Some(Token::NumberLiteral(limit)) = limit_token {
                    if !body.is_empty() {
                        return Err(MDCLError {
                            description: "Unexpected arguments to set_macro_limit",
                            bad_tokens: Vec::from(body),
                        });
                    }
                    if let Ok(limit) = u32::from_str_radix(&*limit, 10) {
                        if limit > 0 {
                            self.macro_limit = limit;
                            Ok(VecDeque::new())
                        } else {
                            Err(name.make_error("Macro limit must be positive"))
                        }
                    } else {
                        Err(name.make_error("Macro limit must be an integer"))
                    }
                } else {
                    Err(name.make_error("Argument to set_macro_limit must be number"))
                }
            }
            "counter" => {
                let counter_name = body.pop_front();
                if let Some(counter) = counter_name {
                    if !body.is_empty() {
                        return Err(MDCLError {
                            description: "Unexpected arguments to counter macro",
                            bad_tokens: Vec::from(body),
                        });
                    }
                    if !self.counters.contains_key(&counter) {
                        self.counters.insert(counter.clone(), 0);
                    } else {
                        *self.counters.get_mut(&counter).unwrap() += 1;
                    }
                    Ok(VecDeque::from(vec![Token::NumberLiteral(Cow::from(
                        self.counters.get(&counter).unwrap().to_string(),
                    ))]))
                } else {
                    Err(name.make_error("The counter macro requires a counter name"))
                }
            }
            "nop" => Ok(body),
            _ => match self.user_macros.get(&name) {
                Some(user_macro) => {
                    let args: Vec<Vec<Token>> = process_macros(Vec::from(body))?
                        .split(|tok| tok == &Token::Punctuation(","))
                        .map(|slice| Vec::from(slice))
                        .collect();
                    if args.len() != user_macro.args.len() {
                        return Err(name.make_error("Incorrect number of macro arguments"));
                    }
                    let mut result = VecDeque::new();
                    for macro_result_token in user_macro.result.clone() {
                        match user_macro.args.get(&macro_result_token) {
                            Some(i) => result.append(&mut VecDeque::from(args[*i].clone())),
                            None => result.push_back(macro_result_token),
                        }
                    }
                    Ok(result)
                }
                None => Err(name.make_error("Unknown macro")),
            },
        }
    }
}

pub fn process_macros<'a>(tokens: Vec<Token<'a>>) -> Result<Vec<Token<'a>>, MDCLError<'a>> {
    let mut state = MacroState::default();
    state.macro_limit = 1000;
    let mut tokens = VecDeque::from(tokens);
    let mut result = Vec::with_capacity(tokens.len());
    while !tokens.is_empty() {
        let token = tokens.pop_front().unwrap();
        if !tokens.is_empty() && tokens.front().unwrap() == &Token::Punctuation("[") {
            if state.macros_seen > state.macro_limit {
                return Err(token.make_error("Macro limit exceeded"));
            }
            state.macros_seen += 1;
            let opening_bracket = tokens.pop_front().unwrap();
            let mut brackets_seen = 1u32;
            let mut args = VecDeque::new();
            while !tokens.is_empty() {
                let arg_token = tokens.pop_front().unwrap();
                if arg_token == Token::Punctuation("[") {
                    brackets_seen += 1;
                } else if arg_token == Token::Punctuation("]") {
                    brackets_seen -= 1;
                    if brackets_seen == 0 {
                        break;
                    }
                }
                args.push_back(arg_token);
            }
            if brackets_seen != 0 {
                return Err(opening_bracket.make_error("Unclosed macro bracket"));
            }
            let mut macro_result = state.do_macro(token, args)?;
            while !macro_result.is_empty() {
                tokens.push_front(macro_result.pop_back().unwrap());
            }
        } else {
            result.push(token);
        }
    }
    Ok(result)
}
