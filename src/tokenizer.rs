use error::HasTokens;
use error::MDCLError;

use std::borrow::Cow;
use std::hash::Hash;
use std::slice;
use std::str;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Token<'a> {
    Identifier(Cow<'a, str>),
    FunctionName(Cow<'a, str>),
    NumberLiteral(Cow<'a, str>),
    Keyword(&'a str),
    Punctuation(&'a str),
    StringStart(&'a str),
    StringEnd(&'a str),
    InterpolationStart(&'a str),
    InterpolationEnd(&'a str),
    StringData(Cow<'a, str>),
}

impl<'a> Token<'a> {
    pub fn contents(&self) -> &str {
        match self {
            Token::Identifier(ref t) => t,
            Token::FunctionName(ref t) => t,
            Token::NumberLiteral(t) => &*t,
            Token::Keyword(ref t) => t,
            Token::Punctuation(ref t) => t,
            Token::StringStart(ref t) => t,
            Token::StringEnd(ref t) => t,
            Token::InterpolationStart(ref t) => t,
            Token::InterpolationEnd(ref t) => t,
            Token::StringData(ref t) => t,
        }
    }
}

trait TokenizableSlice: Sized + Copy + AsRef<str> + Hash + PartialEq + Eq {
    /// Return the first character of the slice, or None if it's empty.
    fn first_char(self) -> Option<char>;

    /// Split this slice into two slices, one containing the first character and one containing the
    /// rest of the characters. Returns None if the slice is empty.
    fn start_token(self) -> Option<(Self, Self)>;

    /// Given two adjacent slices, "pop" a character from the second onto the first. Returns None
    /// if the slice is empty, and panics if the slices aren't adjacent.
    fn continue_token(slices: (Self, Self)) -> Option<(Self, Self)>;
}

/// Returns a slice starting at the first non-whitespace, non-comment character of this slice, and
/// a boolean indicating if any whitespace was skipped.
fn skip_comments_and_whitespace<Slice: TokenizableSlice>(slice: Slice) -> (Slice, bool) {
    let mut result = slice;
    let mut in_comment = false;
    let mut saw_whitespace = false;
    while let Some(next_char) = result.first_char() {
        if next_char == '#' {
            in_comment = true
        } else if next_char == '\n' {
            in_comment = false
        } else if !in_comment && !next_char.is_whitespace() {
            break;
        }
        result = result.start_token().unwrap().1;
        saw_whitespace = true;
    }
    (result, saw_whitespace)
}

/// If this slice starts with an identifier, returns a tuple containing the identifier and
/// the remaining characters.
fn take_identifier<Slice: TokenizableSlice>(slice: Slice) -> Option<(Slice, Slice)> {
    let first_char = slice.first_char()?;
    if !first_char.is_alphabetic() && first_char != '_' {
        return None;
    }
    let mut token_pair = slice.start_token()?;
    while let Some(next_char) = token_pair.1.first_char() {
        if !next_char.is_alphanumeric() && next_char != '_' {
            break;
        }
        token_pair = Slice::continue_token(token_pair).unwrap();
    }
    Some(token_pair)
}

/// If this slice starts with a multi-character operator like `>=`, returns a tuple containing
/// the operator and the remaining characters.
fn take_operator<Slice: TokenizableSlice>(slice: Slice) -> Option<(Slice, Slice)> {
    let result = Slice::continue_token(slice.start_token()?)?;
    if result.0.as_ref() == ">=" || result.0.as_ref() == "<=" || result.0.as_ref() == "**" {
        Some(result)
    } else {
        None
    }
}

/// If this slice starts with a number literal like `4.3`, returns a tuple containing the
/// literal and the remaining characters.
fn take_number_literal<Slice: TokenizableSlice>(slice: Slice) -> Option<(Slice, Slice)> {
    let first_char = slice.first_char()?;
    if !first_char.is_digit(10) && first_char != '-' {
        return None;
    }
    let mut seen_digit = first_char != '-';
    let mut seen_decimal_point = false;
    let mut literal_pair = slice.start_token()?;
    while let Some(next_char) = literal_pair.1.first_char() {
        if next_char == '.' && !seen_decimal_point {
            seen_decimal_point = true;
        } else if !next_char.is_digit(10) {
            break;
        } else {
            seen_digit = true;
        }
        literal_pair = Slice::continue_token(literal_pair).unwrap()
    }
    if seen_digit {
        Some(literal_pair)
    } else {
        None
    }
}

/// If this slice starts with a string literal like `"Hello ${world}", returns a tuple
/// containing a vector of the literal's tokens and the remaining characters.
fn take_string<'a>(slice: &'a str) -> Option<(Vec<Token<'a>>, &'a str)> {
    let first_char = slice.first_char()?;
    if first_char != '"' {
        return None;
    }
    let (first_token, mut remaining_chars) = slice.start_token()?;
    let mut result = vec![Token::StringStart(first_token)];
    while let Some(next_char) = remaining_chars.first_char() {
        match next_char {
            '"' => {
                let last_token_pair = remaining_chars.start_token().unwrap();
                result.push(Token::StringEnd(last_token_pair.0));
                return Some((result, last_token_pair.1));
            }
            '$' => {
                let dollar_pair = remaining_chars.start_token().unwrap();
                if let Some('{') = dollar_pair.1.first_char() {
                    let interp_start_pair = <&'a str>::continue_token(dollar_pair).unwrap();
                    result.push(Token::InterpolationStart(interp_start_pair.0));
                    let more_tokens_pair = match tokenize(interp_start_pair.1) {
                        Ok(tokens) => tokens,
                        Err(_) => return None,
                    };
                    result.extend(more_tokens_pair.0);
                    if let Some(interp_end_pair) = more_tokens_pair.1.start_token() {
                        result.push(Token::InterpolationEnd(interp_end_pair.0));
                        remaining_chars = interp_end_pair.1
                    } else {
                        remaining_chars = more_tokens_pair.1
                    }
                } else {
                    result.push(Token::StringData(Cow::from(dollar_pair.0)));
                    remaining_chars = dollar_pair.1;
                }
            }
            '\\' => {
                let escape_pair = remaining_chars.start_token().unwrap();
                if let Some(escape_pair) = <&'a str>::continue_token(escape_pair) {
                    result.push(Token::StringData(Cow::from(escape_pair.0)));
                    remaining_chars = escape_pair.1;
                } else {
                    result.push(Token::StringData(Cow::from(escape_pair.0)));
                    remaining_chars = escape_pair.1;
                }
            }
            _ => {
                let mut contents_pair = remaining_chars.start_token().unwrap();
                while let Some(next_char) = contents_pair.1.first_char() {
                    if next_char == '"' || next_char == '$' || next_char == '\\' {
                        break;
                    }
                    contents_pair = <&'a str>::continue_token(contents_pair).unwrap()
                }
                result.push(Token::StringData(Cow::from(contents_pair.0)));
                remaining_chars = contents_pair.1
            }
        }
    }
    Some((result, remaining_chars))
}

fn is_keyword(slice: impl AsRef<str>) -> bool {
    match slice.as_ref() {
        "when" => true,
        "WHEN" => true,
        "otherwise" => true,
        "OTHERWISE" => true,
        "and" => true,
        "AND" => true,
        "or" => true,
        "OR" => true,
        "true" => true,
        "TRUE" => true,
        "false" => true,
        "FALSE" => false,
        _ => false,
    }
}

pub fn tokenize<'a>(slice: &'a str) -> Result<(Vec<Token<'a>>, &'a str), MDCLError<'a>> {
    let mut remaining_chars = skip_comments_and_whitespace(slice).0;
    let mut result = vec![];
    let mut braces_seen = 0;
    let mut saw_whitespace = true;
    while let Some(next_char) = remaining_chars.first_char() {
        if let Some(ident_pair) = take_identifier(remaining_chars) {
            result.push(if is_keyword(ident_pair.0) {
                Token::Keyword(ident_pair.0)
            } else if let Some('(') = ident_pair.1.first_char() {
                Token::FunctionName(Cow::from(ident_pair.0))
            } else {
                Token::Identifier(Cow::from(ident_pair.0))
            });
            remaining_chars = ident_pair.1;
        } else if let Some(operator_pair) = take_operator(remaining_chars) {
            result.push(Token::Punctuation(operator_pair.0));
            remaining_chars = operator_pair.1;
        } else if let Some(number_literal_pair) = take_number_literal(remaining_chars) {
            result.push(Token::NumberLiteral(Cow::from(number_literal_pair.0)));
            remaining_chars = number_literal_pair.1;
            if number_literal_pair.0.first_char() == Some('-') && !saw_whitespace {
                return Err(result
                    .pop()
                    .unwrap()
                    .make_error("Ambiguous whitespace around subtraction operator"));
            }
        } else if let Some(string_literal_pair) = take_string(remaining_chars) {
            result.extend(string_literal_pair.0);
            remaining_chars = string_literal_pair.1;
        } else {
            if next_char == '{' {
                braces_seen += 1
            } else if next_char == '}' {
                braces_seen -= 1;
                if braces_seen < 0 {
                    return Ok((result, remaining_chars));
                }
            }
            let symbol_token_pair = remaining_chars.start_token().unwrap();
            remaining_chars = symbol_token_pair.1;
            result.push(Token::Punctuation(symbol_token_pair.0));
        }
        let scw = skip_comments_and_whitespace(remaining_chars);
        remaining_chars = scw.0;
        saw_whitespace = scw.1;
    }
    Ok((result, remaining_chars))
}

fn first_char_length(s: &str) -> Option<usize> {
    if s.is_empty() {
        return None;
    }
    for i in 1..(s.len()) {
        if s.is_char_boundary(i) {
            return Some(i);
        }
    }
    Some(s.len())
}

impl<'a> TokenizableSlice for &'a str {
    fn first_char(self) -> Option<char> {
        self.chars().next()
    }

    fn start_token(self) -> Option<(&'a str, &'a str)> {
        Some(self.split_at(first_char_length(&self)?))
    }

    fn continue_token(slices: (&'a str, &'a str)) -> Option<(&'a str, &'a str)> {
        unsafe {
            assert!(slices.0.as_ptr().add(slices.0.len()) == slices.1.as_ptr());
            let bigslice =
                slice::from_raw_parts(slices.0.as_ptr(), slices.0.len() + slices.1.len());
            let bigstr = str::from_utf8_unchecked(bigslice);
            Some(bigstr.split_at(slices.0.len() + first_char_length(slices.1)?))
        }
    }
}
