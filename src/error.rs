use tokenizer::Token;

#[derive(Debug)]
pub struct MDCLError<'a> {
    pub description: &'static str,
    pub bad_tokens: Vec<Token<'a>>,
}

pub trait HasTokens<'a> {
    fn get_tokens(&self) -> Vec<Token<'a>>;
    fn make_error(&self, description: &'static str) -> MDCLError<'a> {
        MDCLError {
            description,
            bad_tokens: self.get_tokens(),
        }
    }
}

impl<'a> HasTokens<'a> for Token<'a> {
    fn get_tokens(&self) -> Vec<Token<'a>> {
        vec![self.clone()]
    }
}
