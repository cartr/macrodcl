use error::HasTokens;
use error::MDCLError;
use std::iter::Peekable;
use tokenizer::Token;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum StringLiteralComponent<'a> {
    StringData(Token<'a>),
    Interpolation(Expression<'a>),
}

impl<'a> HasTokens<'a> for StringLiteralComponent<'a> {
    fn get_tokens(&self) -> Vec<Token<'a>> {
        match self {
            StringLiteralComponent::StringData(token) => token.get_tokens(),
            StringLiteralComponent::Interpolation(expression) => expression.get_tokens(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Sink<'a> {
    pub component: Option<Token<'a>>,
    pub sink: Token<'a>,
    pub args: Option<Vec<Expression<'a>>>,
}

impl<'a> HasTokens<'a> for Sink<'a> {
    fn get_tokens(&self) -> Vec<Token<'a>> {
        let mut tokens = vec![];
        if let Some(ref component) = self.component {
            tokens.push(component.clone());
        }
        tokens.push(self.sink.clone());
        if let Some(ref args) = self.args {
            for arg in args {
                tokens.append(&mut arg.get_tokens());
            }
        }
        tokens
    }
}

/**
 * Represents an expression. Expressions give values, which can then be used in variable
 * definitions, sink values, or as sub-expressions of a larger expression.
 */
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Expression<'a> {
    StringLiteral(Vec<StringLiteralComponent<'a>>),
    Literal(Token<'a>),
    FunctionEvaluation {
        name: Token<'a>,
        args: Vec<Expression<'a>>,
    },
    Variable(Token<'a>),
    Source {
        component: Token<'a>,
        source: Token<'a>,
        args: Option<Vec<Expression<'a>>>,
    },
    WhenExpression {
        arms: Vec<(Expression<'a>, Expression<'a>)>,
        otherwise: Box<Expression<'a>>,
    },
    Operation {
        lhs: Box<Expression<'a>>,
        operation: Token<'a>,
        rhs: Box<Expression<'a>>,
    },
    /// `Undefined` is used to fill in the `otherwise` field when we've seen a variable
    /// definition in a when-block but not in the surrounding scope. Well-formed Macro DCL code
    /// will never have an `Undefined` expression in it.
    Undefined,
}

impl<'a> HasTokens<'a> for Expression<'a> {
    fn get_tokens(&self) -> Vec<Token<'a>> {
        match self {
            Expression::StringLiteral(components) => {
                let mut tokens = vec![];
                for component in components {
                    tokens.append(&mut component.get_tokens())
                }
                tokens
            }
            Expression::Literal(token) => token.get_tokens(),
            Expression::FunctionEvaluation { name, args } => {
                let mut tokens = vec![name.clone()];
                for arg in args {
                    tokens.append(&mut arg.get_tokens())
                }
                tokens
            }
            Expression::Variable(token) => token.get_tokens(),
            Expression::Source {
                component,
                source,
                args,
            } => {
                let mut tokens = vec![component.clone(), source.clone()];
                if let Some(args) = args {
                    for arg in args {
                        tokens.append(&mut arg.get_tokens())
                    }
                }
                tokens
            }
            Expression::WhenExpression { arms, otherwise } => {
                let mut tokens = vec![];
                for arm in arms {
                    tokens.append(&mut arm.0.get_tokens());
                    tokens.append(&mut arm.1.get_tokens());
                }
                tokens.append(&mut otherwise.get_tokens());
                tokens
            }
            Expression::Operation {
                lhs,
                operation,
                rhs,
            } => {
                let mut tokens = lhs.get_tokens();
                tokens.push(operation.clone());
                tokens.append(&mut rhs.get_tokens());
                tokens
            }
            Expression::Undefined => vec![],
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Statement<'a> {
    VariableAssignment {
        name: Token<'a>,
        value: Expression<'a>,
    },
    SinkAssignment {
        sink: Sink<'a>,
        value: Expression<'a>,
    },
    /**
     * MacroDCL extension. Example:
     *
     * ```mdcl
     * studentFeedback: ""
     * teacherFeedback: ""
     * when correct {
     *     studentFeedback: "You did it!"
     *     teacherFeedback: "Correct"
     * }
     * ```
     *
     * This is equivalent to the following DCL code:
     *
     * ```dcl
     * studentFeedback: when correct "You did it!" otherwise ""
     * teacherFeedback: when correct "Correct" otherwise ""
     * ```
     */
    WhenBlock {
        condition: Expression<'a>,
        contents: Vec<Statement<'a>>,
    },
}

impl<'a> HasTokens<'a> for Statement<'a> {
    fn get_tokens(&self) -> Vec<Token<'a>> {
        match self {
            Statement::VariableAssignment { name, value } => {
                let mut tokens = name.get_tokens();
                tokens.append(&mut value.get_tokens());
                tokens
            }
            Statement::SinkAssignment { sink, value } => {
                let mut tokens = sink.get_tokens();
                tokens.append(&mut value.get_tokens());
                tokens
            }
            Statement::WhenBlock {
                condition,
                contents: _,
            } => condition.get_tokens(),
        }
    }
}

macro_rules! next_or_eof {
    ($tokens:expr) => {
        if let Some(next) = ($tokens).next() {
            next
        } else {
            return Err(MDCLError {
                description: "Unexpected end-of-file",
                bad_tokens: vec![],
            });
        }
    };
    ($tokens:expr, $while_parsing:expr) => {
        if let Some(next) = ($tokens).next() {
            next
        } else {
            return Err(MDCLError {
                description: concat!("Unexpected end-of-file while parsing ", $while_parsing),
                bad_tokens: vec![],
            });
        }
    };
}

macro_rules! peek_or_eof {
    ($tokens:expr) => {
        if let Some(next) = ($tokens).peek() {
            next
        } else {
            return Err(MDCLError {
                description: "Unexpected end-of-file",
                bad_tokens: vec![],
            });
        }
    };
    ($tokens:expr, $while_parsing:expr) => {
        if let Some(next) = ($tokens).peek() {
            next
        } else {
            return Err(MDCLError {
                description: concat!("Unexpected end-of-file while parsing ", $while_parsing),
                bad_tokens: vec![],
            });
        }
    };
}

macro_rules! expect_token {
    ($tokens:expr, $token:pat, $expected_diag:expr, $while_parsing:expr) => {
        let next_token = next_or_eof!($tokens, $while_parsing);
        if let $token = next_token {
        } else {
            return Err(next_token.make_error(concat!(
                "Expected ",
                $expected_diag,
                " while parsing ",
                $while_parsing
            )));
        }
    };
}

fn take_argument_list<'a>(
    tokens: &mut Peekable<impl Iterator<Item = Token<'a>>>,
) -> Result<Vec<Expression<'a>>, MDCLError<'a>> {
    let mut args = vec![];
    let mut next_token = next_or_eof!(tokens, "argument list");
    if next_token != Token::Punctuation("(") {
        return Err(next_token.make_error("Expected opening parenthesis after function name"));
    }
    while next_token != Token::Punctuation(")") {
        if next_token != Token::Punctuation(",")
            && !(next_token == Token::Punctuation("(") && args.len() == 0)
        {
            return Err(next_token.make_error("Unexpected token while parsing argument list"));
        }
        args.push(take_expression(tokens)?);
        next_token = next_or_eof!(tokens, "argument list");
    }
    Ok(args)
}

/**
 * Take a literal, function invokation, variable/source name, when expression, or
 * parenthesized sub-expression from the token stream.
 */
fn take_value<'a>(
    tokens: &mut Peekable<impl Iterator<Item = Token<'a>>>,
) -> Result<Expression<'a>, MDCLError<'a>> {
    let first_token = next_or_eof!(tokens);
    Ok(match first_token {
        Token::StringStart(_) => {
            let mut components = vec![];
            let mut next_token = next_or_eof!(tokens, "string literal");
            loop {
                match next_token {
                    Token::StringData(_) => {
                        components.push(StringLiteralComponent::StringData(next_token))
                    }
                    Token::InterpolationStart(_) => {
                        components.push(StringLiteralComponent::Interpolation(take_expression(
                            tokens,
                        )?));
                        expect_token!(
                            tokens,
                            Token::InterpolationEnd(_),
                            "end of interpolation",
                            "string literal"
                        );
                    }
                    Token::StringEnd(_) => break,
                    _ => {
                        return Err(
                            next_token.make_error("Unexpected token while parsing string literal")
                        )
                    }
                }
                next_token = next_or_eof!(tokens, "string literal");
            }
            Expression::StringLiteral(components)
        }
        Token::NumberLiteral(_) => Expression::Literal(first_token),
        Token::FunctionName(_) => Expression::FunctionEvaluation {
            name: first_token,
            args: take_argument_list(tokens)?,
        },
        Token::Identifier(_) => {
            let has_dot = match tokens.peek() {
                Option::Some(Token::Punctuation(punct)) => punct == &".",
                _ => false,
            };
            if has_dot {
                tokens.next(); // Consume the dot
                let after_dot = next_or_eof!(tokens, "source reference");
                match after_dot {
                    Token::Identifier(_) => Expression::Source {
                        component: first_token,
                        source: after_dot,
                        args: None,
                    },
                    Token::FunctionName(_) => Expression::Source {
                        component: first_token,
                        source: after_dot,
                        args: Some(take_argument_list(tokens)?),
                    },
                    _ => return Err(after_dot.make_error("Expected an identifier after the dot")),
                }
            } else {
                Expression::Variable(first_token)
            }
        }
        Token::Keyword("when") | Token::Keyword("WHEN") => {
            let mut arms = vec![];
            loop {
                let condition = take_expression(tokens)?;
                let result = take_expression(tokens)?;
                arms.push((condition, result));
                let token = next_or_eof!(tokens, "when expression");
                match token {
                    Token::Keyword("when") | Token::Keyword("WHEN") => {}
                    Token::Keyword("otherwise") | Token::Keyword("OTHERWISE") => break,
                    _ => return Err(token.make_error("Expected \"when\" or \"otherwise\" here")),
                }
            }
            Expression::WhenExpression {
                arms: arms,
                otherwise: Box::new(take_expression(tokens)?),
            }
        }
        Token::Keyword("true")
        | Token::Keyword("TRUE")
        | Token::Keyword("false")
        | Token::Keyword("FALSE") => Expression::Literal(first_token),
        Token::Punctuation("(") => {
            let parenthesized = take_expression(tokens)?;
            expect_token!(
                tokens,
                Token::Punctuation(")"),
                "closing parenthesis",
                "parenthesized expression"
            );
            parenthesized
        }
        _ => return Err(first_token.make_error("Unexpected token while parsing expression")),
    })
}

pub fn operator_precedence(token: Token) -> Option<u32> {
    match token {
        Token::Punctuation("**") => Some(5),
        Token::Punctuation("*") | Token::Punctuation("/") => Some(4),
        Token::Punctuation("+") | Token::Punctuation("-") => Some(3),
        Token::Punctuation(">")
        | Token::Punctuation("<")
        | Token::Punctuation(">=")
        | Token::Punctuation("<=")
        | Token::Punctuation("=") => Some(2),
        Token::Keyword("and")
        | Token::Keyword("AND")
        | Token::Keyword("or")
        | Token::Keyword("OR") => Some(1),
        _ => None,
    }
}

fn operator_precedence_peek(token: Option<&Token>) -> Option<u32> {
    operator_precedence(token?.clone())
}

/*
 * Operator-precedence parser ripped straight from
 * https://en.wikipedia.org/wiki/Operator-precedence_parser#Pseudo-code
 */
fn take_expression_helper<'a>(
    tokens: &mut Peekable<impl Iterator<Item = Token<'a>>>,
    lhs: Expression<'a>,
    min_precedence: u32,
) -> Result<Expression<'a>, MDCLError<'a>> {
    let mut result = lhs;
    while let Some(precedence) = operator_precedence_peek(tokens.peek()) {
        if precedence < min_precedence {
            break;
        }
        let operator = next_or_eof!(tokens);
        let mut rhs = take_value(tokens)?;
        while let Some(rhs_operator_precedence) = operator_precedence_peek(tokens.peek()) {
            if rhs_operator_precedence < precedence {
                break;
            }
            rhs = take_expression_helper(tokens, rhs, rhs_operator_precedence)?;
        }
        result = Expression::Operation {
            lhs: Box::new(result),
            operation: operator,
            rhs: Box::new(rhs),
        };
    }
    Ok(result)
}

pub fn take_expression<'a>(
    tokens: &mut Peekable<impl Iterator<Item = Token<'a>>>,
) -> Result<Expression<'a>, MDCLError<'a>> {
    let lhs = take_value(tokens)?;
    take_expression_helper(tokens, lhs, 0)
}

pub fn take_statement<'a>(
    tokens: &mut Peekable<impl Iterator<Item = Token<'a>>>,
) -> Result<Statement<'a>, MDCLError<'a>> {
    let first_token = next_or_eof!(tokens);
    match first_token {
        Token::Keyword("when") | Token::Keyword("WHEN") => {
            let condition = take_expression(tokens)?;
            expect_token!(
                tokens,
                Token::Punctuation("{"),
                "opening curly brace",
                "when block"
            );
            let mut contents = vec![];
            while peek_or_eof!(tokens, "when block") != &Token::Punctuation("}") {
                contents.push(take_statement(tokens)?);
            }
            tokens.next(); // Take the closing curly brace
            Ok(Statement::WhenBlock {
                condition: condition,
                contents: contents,
            })
        }
        Token::FunctionName(_) => {
            let args = take_argument_list(tokens)?;
            expect_token!(
                tokens,
                Token::Punctuation(":"),
                "colon",
                "sink assignment statement"
            );
            let value = take_expression(tokens)?;
            Ok(Statement::SinkAssignment {
                sink: Sink {
                    component: None,
                    sink: first_token,
                    args: Some(args),
                },
                value: value,
            })
        }
        Token::Identifier(_) => {
            let after_ident = next_or_eof!(tokens);
            match after_ident {
                Token::Punctuation("=") => Ok(Statement::VariableAssignment {
                    name: first_token,
                    value: take_expression(tokens)?,
                }),
                Token::Punctuation(":") => Ok(Statement::SinkAssignment {
                    sink: Sink {
                        component: None,
                        sink: first_token,
                        args: None,
                    },
                    value: take_expression(tokens)?,
                }),
                Token::Punctuation(".") => {
                    let after_dot = next_or_eof!(tokens, "sink assignment statement");
                    match after_dot {
                        Token::Identifier(_) => {
                            expect_token!(
                                tokens,
                                Token::Punctuation(":"),
                                "colon or parenthesis",
                                "sink assignment statement"
                            );
                            Ok(Statement::SinkAssignment {
                                sink: Sink {
                                    component: Some(first_token),
                                    sink: after_dot,
                                    args: None,
                                },
                                value: take_expression(tokens)?,
                            })
                        }
                        Token::FunctionName(_) => {
                            let args = take_argument_list(tokens)?;
                            expect_token!(
                                tokens,
                                Token::Punctuation(":"),
                                "colon",
                                "sink assignment statement"
                            );
                            Ok(Statement::SinkAssignment {
                                sink: Sink {
                                    component: Some(first_token),
                                    sink: after_dot,
                                    args: Some(args),
                                },
                                value: take_expression(tokens)?,
                            })
                        }
                        _ => Err(after_dot.make_error(
                            "Expected an identifier while parsing sink assignment statement",
                        )),
                    }
                }
                _ => Err(after_ident.make_error("Unexpected token")),
            }
        }
        _ => Err(first_token
            .make_error("Top-level statements must be variable/sink assignments or when blocks")),
    }
}

/**
 * Parse the given program, returning a vector of the Statements in it.
 */
pub fn parse_program<'a>(
    tokens: impl IntoIterator<Item = Token<'a>>,
) -> Result<Vec<Statement<'a>>, MDCLError<'a>> {
    let mut tokens = tokens.into_iter().peekable();
    let mut result = vec![];
    while tokens.peek().is_some() {
        result.push(take_statement(&mut tokens)?);
    }
    Ok(result)
}
