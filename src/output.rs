use error::HasTokens;
use error::MDCLError;
use parser::Expression;
use parser::Statement;
use parser::StringLiteralComponent;
use tokenizer::Token;

use std::borrow::Cow;

pub trait DCLOutputtable<'b> {
    fn to_dcl_tokens<'a>(&'a self) -> Result<Vec<Token<'b>>, MDCLError<'b>>;
}

fn is_latex_operator(token: &Token) -> bool {
    match token {
        Token::Punctuation("+")
        | Token::Punctuation("-")
        | Token::Punctuation("*")
        | Token::Punctuation("/")
        | Token::Punctuation("**") => true,
        _ => false,
    }
}

fn is_associative<'a, 'b>(token: &'a Token<'b>) -> bool {
    match token {
        Token::Keyword("and")
        | Token::Keyword("AND")
        | Token::Keyword("or")
        | Token::Keyword("OR") => true,
        _ => false,
    }
}

fn dcl_precedence<'a, 'b>(token: &'a Token<'b>) -> Result<u32, MDCLError<'b>> {
    if is_latex_operator(token) {
        Ok(999)
    } else {
        match token {
            Token::Punctuation(">")
            | Token::Punctuation("<")
            | Token::Punctuation(">=")
            | Token::Punctuation("<=")
            | Token::Punctuation("=") => Ok(2),
            Token::Keyword("and")
            | Token::Keyword("AND")
            | Token::Keyword("or")
            | Token::Keyword("OR") => Ok(1),
            _ => Err(token.make_error("Unsupported operator")),
        }
    }
}

fn push_args_list<'a, 'b>(
    tokens: &mut Vec<Token<'b>>,
    args: &'a Vec<Expression<'b>>,
) -> Result<(), MDCLError<'b>> {
    tokens.push(Token::Punctuation("("));
    for i in 0..args.len() {
        tokens.append(&mut args[i].to_dcl_tokens()?);
        if i < args.len() - 1 {
            tokens.push(Token::Punctuation(","));
        }
    }
    tokens.push(Token::Punctuation(")"));
    Ok(())
}

fn side_needs_parens<'a>(
    operation: &Token<'a>,
    side: &Expression<'a>,
    is_rhs: bool,
) -> Result<bool, MDCLError<'a>> {
    match side {
        Expression::WhenExpression {
            arms: _,
            otherwise: _,
        } => Ok(true),
        Expression::Operation {
            lhs: _,
            rhs: _,
            operation: side_operation,
        } => Ok(!(operation == side_operation && is_associative(operation))
            && dcl_precedence(operation)? >= dcl_precedence(side_operation)?),
        _ => Ok(false),
    }
}

impl<'b> DCLOutputtable<'b> for Expression<'b> {
    fn to_dcl_tokens<'a>(&'a self) -> Result<Vec<Token<'b>>, MDCLError<'b>> {
        Ok(match self {
            Expression::StringLiteral(components) => {
                let mut tokens = vec![Token::StringStart("\"")];
                for component in components {
                    tokens.append(&mut component.to_dcl_tokens()?);
                }
                tokens.push(Token::StringEnd("\""));
                tokens
            }
            Expression::Literal(token) | Expression::Variable(token) => vec![token.clone()],
            Expression::FunctionEvaluation { name, args } => {
                let mut tokens = vec![name.clone()];
                push_args_list(&mut tokens, args)?;
                tokens
            }
            Expression::Source {
                component,
                source,
                args,
            } => {
                let mut tokens = vec![component.clone(), Token::Punctuation("."), source.clone()];
                if let Some(ref args) = args {
                    push_args_list(&mut tokens, args)?;
                }
                tokens
            }
            Expression::WhenExpression { arms, otherwise } => {
                let mut tokens = vec![];
                for arm in arms {
                    tokens.push(Token::Keyword("when"));
                    tokens.append(&mut arm.0.to_dcl_tokens()?);
                    tokens.append(&mut arm.1.to_dcl_tokens()?);
                }
                tokens.push(Token::Keyword("otherwise"));
                tokens.append(&mut otherwise.to_dcl_tokens()?);
                tokens
            }
            Expression::Operation {
                lhs,
                operation,
                rhs,
            } => {
                let mut tokens = vec![];
                if is_latex_operator(&operation) {
                    tokens.push(Token::FunctionName(Cow::from("numericValue")));
                    tokens.push(Token::Punctuation("("));
                    tokens.push(Token::StringStart("\""));
                    tokens.push(Token::InterpolationStart("${"));
                    tokens.append(&mut lhs.to_dcl_tokens()?);
                    tokens.push(Token::InterpolationEnd("}"));
                    tokens.push(Token::StringData(Cow::from(" ")));
                    tokens.push(Token::StringData(Cow::from(String::from(
                        if operation.contents() == "**" {
                            "^{"
                        } else {
                            operation.contents()
                        },
                    ))));
                    tokens.push(Token::StringData(Cow::from(" ")));
                    tokens.push(Token::InterpolationStart("${"));
                    tokens.append(&mut rhs.to_dcl_tokens()?);
                    tokens.push(Token::InterpolationEnd("}"));
                    if operation.contents() == "**" {
                        tokens.push(Token::StringData(Cow::from("}")));
                    }
                    tokens.push(Token::StringEnd("\""));
                    tokens.push(Token::Punctuation(")"));
                } else {
                    dcl_precedence(operation)?; // Ensure operator is valid
                    if side_needs_parens(operation, lhs, false)? {
                        tokens.push(Token::Punctuation("("));
                        tokens.append(&mut lhs.to_dcl_tokens()?);
                        tokens.push(Token::Punctuation(")"));
                    } else {
                        tokens.append(&mut lhs.to_dcl_tokens()?);
                    }
                    tokens.push(operation.clone());
                    if side_needs_parens(operation, rhs, true)? {
                        tokens.push(Token::Punctuation("("));
                        tokens.append(&mut rhs.to_dcl_tokens()?);
                        tokens.push(Token::Punctuation(")"));
                    } else {
                        tokens.append(&mut rhs.to_dcl_tokens()?);
                    }
                }
                tokens
            }
            Expression::Undefined => {
                return Err(MDCLError {
                    description: "Tried to output undefined expression",
                    bad_tokens: vec![],
                });
            }
        })
    }
}

impl<'b> DCLOutputtable<'b> for Statement<'b> {
    fn to_dcl_tokens<'a>(&'a self) -> Result<Vec<Token<'b>>, MDCLError<'b>> {
        Ok(match self {
            Statement::VariableAssignment { name, value } => {
                let mut tokens = vec![name.clone(), Token::Punctuation("=")];
                tokens.append(&mut value.to_dcl_tokens()?);
                tokens
            }
            Statement::SinkAssignment { sink, value } => {
                let mut tokens = vec![];
                if let Some(ref component) = sink.component {
                    tokens.push(component.clone());
                    tokens.push(Token::Punctuation("."));
                }
                tokens.push(sink.sink.clone());
                if let Some(ref args) = sink.args {
                    push_args_list(&mut tokens, &args)?;
                }
                tokens.push(Token::Punctuation(":"));
                tokens.append(&mut value.to_dcl_tokens()?);
                tokens
            }
            Statement::WhenBlock {
                condition: _,
                contents: _,
            } => return Err(self.make_error("When-blocks aren't valid DCL.")),
        })
    }
}

impl<'b> DCLOutputtable<'b> for StringLiteralComponent<'b> {
    fn to_dcl_tokens<'a>(&'a self) -> Result<Vec<Token<'b>>, MDCLError<'b>> {
        Ok(match self {
            StringLiteralComponent::StringData(token) => vec![token.clone()],
            StringLiteralComponent::Interpolation(expression) => {
                let mut tokens = vec![Token::InterpolationStart("${")];
                tokens.append(&mut expression.to_dcl_tokens()?);
                tokens.push(Token::InterpolationEnd("}"));
                tokens
            }
        })
    }
}
