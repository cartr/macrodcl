use error::HasTokens;
use error::MDCLError;
use parser::Expression;
use parser::Sink;
use parser::Statement;
use parser::StringLiteralComponent;
use tokenizer::Token;

use std::borrow::Cow;
use std::collections::HashMap;
use std::collections::HashSet;
use std::str::FromStr;

use petgraph::algo::toposort;
use petgraph::unionfind::UnionFind;
use petgraph::visit::EdgeRef;
use petgraph::Graph;

fn merge_when<'a>(
    current_value: &mut Expression<'a>,
    condition: &Expression<'a>,
    when_value: &Expression<'a>,
) {
    let mut new_arms = if let Expression::WhenExpression { arms, otherwise } = when_value.clone() {
        if otherwise.as_ref() == &Expression::Undefined {
            let mut new_arms = arms.clone();
            for arm in new_arms.iter_mut() {
                arm.0 = Expression::Operation {
                    lhs: Box::new(condition.clone()),
                    operation: Token::Keyword("and"),
                    rhs: Box::new(arm.0.clone()),
                }
            }
            new_arms
        } else {
            vec![(condition.clone(), when_value.clone())]
        }
    } else {
        vec![(condition.clone(), when_value.clone())]
    };
    match current_value {
        Expression::WhenExpression { arms, otherwise: _ } => {
            while !new_arms.is_empty() {
                arms.insert(0, new_arms.pop().unwrap());
            }
            return;
        }
        _ => (),
    }
    *current_value = Expression::WhenExpression {
        arms: new_arms,
        otherwise: Box::new(current_value.clone()),
    };
}

fn has_undefined(expr: &Expression) -> bool {
    match expr {
        Expression::StringLiteral(components) => {
            components.into_iter().any(|component| match component {
                StringLiteralComponent::StringData(_) => false,
                StringLiteralComponent::Interpolation(expr) => has_undefined(expr),
            })
        }
        Expression::Literal(_) => false,
        Expression::FunctionEvaluation { name: _, args } => {
            args.into_iter().any(|arg| has_undefined(&arg))
        }
        Expression::Variable(_) => false,
        Expression::Source {
            component: _,
            source: _,
            args,
        } => if let Some(args) = args {
            args.into_iter().any(|arg| has_undefined(arg))
        } else {
            false
        },
        Expression::WhenExpression { arms, otherwise } => {
            arms.into_iter()
                .any(|arm| has_undefined(&arm.0) || has_undefined(&arm.1))
                || has_undefined(&*otherwise)
        }
        Expression::Operation {
            lhs,
            operation: _,
            rhs,
        } => has_undefined(&*lhs) || has_undefined(&*rhs),
        Expression::Undefined => true,
    }
}

/**
 * Remove all when-blocks from the given program, replacing them with top-level when-expressions.
 */
pub fn unblock_whens<'a>(
    program: &Vec<Statement<'a>>,
) -> Result<Vec<Statement<'a>>, MDCLError<'a>> {
    let program = unblock_whens_helper(program)?;
    for statement in &program {
        match statement {
            Statement::VariableAssignment { name, value } => if has_undefined(&value) {
                return Err(name.make_error("Variable is only defined inside a when block"));
            },
            Statement::SinkAssignment { sink, value } => if has_undefined(&value) {
                return Err(sink.make_error("Sink is only defined inside a when block"));
            },
            Statement::WhenBlock {
                condition,
                contents: _,
            } => return Err(condition.make_error("Failed to remove when block")),
        }
    }
    Ok(program)
}
pub fn unblock_whens_helper<'a>(
    program: &Vec<Statement<'a>>,
) -> Result<Vec<Statement<'a>>, MDCLError<'a>> {
    let mut variables = HashMap::new();
    let mut sinks = HashMap::new();
    for statement in program {
        match statement {
            Statement::VariableAssignment { name, value } => {
                match variables.get_mut(name) {
                    Some(Expression::WhenExpression { arms: _, otherwise }) => {
                        if otherwise.as_ref() == &Expression::Undefined {
                            *otherwise = Box::new(value.clone());
                        } else {
                            return Err(name.make_error(
                                "This variable was already defined in this scope",
                            ));
                        }
                    }
                    Some(_) => {
                        return Err(name.make_error(
                            "This variable was already defined in this scope",
                        ))
                    }
                    None => {}
                }
                if !variables.contains_key(name) {
                    variables.insert(name.clone(), value.clone());
                }
            }
            Statement::SinkAssignment { sink, value } => {
                match sinks.get_mut(sink) {
                    Some(Expression::WhenExpression { arms: _, otherwise }) => {
                        if otherwise.as_ref() == &Expression::Undefined {
                            *otherwise = Box::new(value.clone());
                        } else {
                            return Err(sink.make_error(
                                "This variable was already defined in this scope",
                            ));
                        }
                    }
                    Some(_) => {
                        return Err(sink.make_error(
                            "This variable was already defined in this scope",
                        ))
                    }
                    None => {}
                }
                if !sinks.contains_key(sink) {
                    sinks.insert(sink.clone(), value.clone());
                }
            }
            Statement::WhenBlock {
                condition,
                contents,
            } => {
                let when_statements = unblock_whens_helper(&contents)?;
                for when_statement in when_statements {
                    match when_statement {
                        Statement::VariableAssignment { name, value } => {
                            if !variables.contains_key(&name) {
                                variables.insert(name.clone(), Expression::Undefined);
                            }
                            merge_when(variables.get_mut(&name).unwrap(), &condition, &value);
                        }
                        Statement::SinkAssignment { sink, value } => {
                            if !sinks.contains_key(&sink) {
                                sinks.insert(sink.clone(), Expression::Undefined);
                            }
                            merge_when(sinks.get_mut(&sink).unwrap(), &condition, &value);
                        }
                        _ => {
                            return Err(when_statement
                                .make_error("Unrecognized statement inside when-block"))
                        }
                    }
                }
            }
        }
    }
    let mut result = vec![];
    for (name, value) in variables {
        result.push(Statement::VariableAssignment { name, value });
    }
    for (sink, value) in sinks {
        result.push(Statement::SinkAssignment { sink, value });
    }
    Ok(result)
}

enum MaybeLiteral<'a> {
    Number(f64),
    Boolean(bool),
    NotLiteral(Expression<'a>),
}

impl<'a> From<Expression<'a>> for MaybeLiteral<'a> {
    fn from(expression: Expression<'a>) -> MaybeLiteral<'a> {
        match &expression {
            Expression::Literal(Token::Keyword("true"))
            | Expression::Literal(Token::Keyword("TRUE")) => return MaybeLiteral::Boolean(true),
            Expression::Literal(Token::Keyword("false"))
            | Expression::Literal(Token::Keyword("FALSE")) => return MaybeLiteral::Boolean(false),
            Expression::Literal(Token::NumberLiteral(val)) => {
                return match f64::from_str(&*val) {
                    Ok(val) => MaybeLiteral::Number(val),
                    Err(_) => MaybeLiteral::NotLiteral(expression.clone()),
                }
            }
            _ => (),
        }
        MaybeLiteral::NotLiteral(expression)
    }
}

impl<'a> Into<Expression<'a>> for MaybeLiteral<'a> {
    fn into(self) -> Expression<'a> {
        match self {
            MaybeLiteral::Number(val) => {
                Expression::Literal(Token::NumberLiteral(Cow::from(val.to_string())))
            }
            MaybeLiteral::Boolean(true) => Expression::Literal(Token::Keyword("true")),
            MaybeLiteral::Boolean(false) => Expression::Literal(Token::Keyword("false")),
            MaybeLiteral::NotLiteral(expression) => expression,
        }
    }
}

fn fold_constants_expr(expr: &mut Expression) {
    let mut should_replace_with = None;
    match expr {
        Expression::StringLiteral(components) => {
            let mut new_components = vec![];
            for component in components {
                let adding_components = match component {
                    StringLiteralComponent::StringData(_) => vec![component.clone()],
                    StringLiteralComponent::Interpolation(expr) => {
                        let mut new_expr = expr.clone();
                        fold_constants_expr(&mut new_expr);
                        match new_expr {
                            Expression::Literal(value) => {
                                vec![StringLiteralComponent::StringData(value)]
                            }
                            Expression::StringLiteral(components) => components,
                            _ => vec![StringLiteralComponent::Interpolation(new_expr)],
                        }
                    }
                };
                for new_component in adding_components {
                    let mut should_push = true;
                    if let (
                        Some(StringLiteralComponent::StringData(Token::StringData(val1))),
                        StringLiteralComponent::StringData(val2),
                    ) = (new_components.last_mut(), &new_component)
                    {
                        val1.to_mut().push_str(val2.contents());
                        should_push = false;
                    }
                    if should_push {
                        new_components.push(new_component);
                    }
                }
            }
            should_replace_with = Some(Expression::StringLiteral(new_components))
        }
        Expression::Undefined
        | Expression::Literal(_)
        | Expression::Variable(_)
        | Expression::Source {
            component: _,
            source: _,
            args: None,
        } => (),
        Expression::FunctionEvaluation { name: _, args }
        | Expression::Source {
            component: _,
            source: _,
            args: Some(args),
        } => for arg in args.iter_mut() {
            fold_constants_expr(arg)
        },
        Expression::WhenExpression { arms, otherwise } => {
            fold_constants_expr(&mut *otherwise);
            for arm in arms.iter_mut() {
                fold_constants_expr(&mut arm.0);
                fold_constants_expr(&mut arm.1);
            }
            let mut i = 0;
            while i != arms.len() {
                if &arms[i].0 == &**otherwise {
                    arms.remove(i);
                } else if let MaybeLiteral::Boolean(condition) =
                    MaybeLiteral::from(arms[i].0.clone())
                {
                    if condition {
                        **otherwise = arms[i].1.clone();
                        arms.truncate(i);
                        break;
                    } else {
                        arms.remove(i);
                    }
                } else if i > 0 && arms[i].1 == arms[i - 1].1 {
                    let old_arm = arms.remove(i);
                    arms[i - 1].0 = Expression::Operation {
                        lhs: Box::new(arms[i - 1].0.clone()),
                        operation: Token::Keyword("or"),
                        rhs: Box::new(old_arm.0),
                    };
                    fold_constants_expr(&mut arms[i - 1].0);
                } else {
                    i += 1;
                }
            }
            if arms.len() == 0 {
                should_replace_with = Some(otherwise.as_ref().clone());
            } else if arms.len() == 1 {
                match (
                    MaybeLiteral::from(arms[0].1.clone()),
                    MaybeLiteral::from(otherwise.as_ref().clone()),
                ) {
                    (MaybeLiteral::Boolean(true), MaybeLiteral::Boolean(false)) => {
                        should_replace_with = Some(arms[0].0.clone());
                    }
                    (MaybeLiteral::Boolean(false), MaybeLiteral::Boolean(true)) => {
                        should_replace_with = Some(Expression::FunctionEvaluation {
                            name: Token::Identifier(Cow::from("not")),
                            args: vec![arms[0].0.clone()],
                        });
                    }
                    _ => (),
                }
            }
        }
        Expression::Operation {
            lhs,
            operation,
            rhs,
        } => {
            fold_constants_expr(lhs);
            fold_constants_expr(rhs);
            should_replace_with = match (
                MaybeLiteral::from(lhs.as_ref().clone()),
                operation.contents(),
                MaybeLiteral::from(rhs.as_ref().clone()),
            ) {
                (MaybeLiteral::Number(lhs), "+", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Number(lhs + rhs).into())
                }
                (MaybeLiteral::Number(lhs), "-", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Number(lhs - rhs).into())
                }
                (MaybeLiteral::Number(lhs), "*", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Number(lhs * rhs).into())
                }
                (MaybeLiteral::Number(lhs), "/", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Number(lhs / rhs).into())
                }
                (MaybeLiteral::Number(lhs), "**", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Number(lhs.powf(rhs)).into())
                }
                (MaybeLiteral::Number(lhs), ">", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Boolean(lhs > rhs).into())
                }
                (MaybeLiteral::Number(lhs), "<", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Boolean(lhs < rhs).into())
                }
                (MaybeLiteral::Number(lhs), ">=", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Boolean(lhs >= rhs).into())
                }
                (MaybeLiteral::Number(lhs), "<=", MaybeLiteral::Number(rhs)) => {
                    Some(MaybeLiteral::Boolean(lhs <= rhs).into())
                }
                (MaybeLiteral::Boolean(false), "and", _)
                | (MaybeLiteral::Boolean(false), "AND", _)
                | (_, "and", MaybeLiteral::Boolean(false))
                | (_, "AND", MaybeLiteral::Boolean(false)) => {
                    Some(MaybeLiteral::Boolean(false).into())
                }
                (MaybeLiteral::Boolean(true), "or", _)
                | (MaybeLiteral::Boolean(true), "OR", _)
                | (_, "or", MaybeLiteral::Boolean(true))
                | (_, "OR", MaybeLiteral::Boolean(true)) => {
                    Some(MaybeLiteral::Boolean(true).into())
                }
                (MaybeLiteral::Boolean(true), "and", other)
                | (other, "and", MaybeLiteral::Boolean(true))
                | (MaybeLiteral::Boolean(false), "or", other)
                | (other, "or", MaybeLiteral::Boolean(false)) => Some(other.into()),
                (MaybeLiteral::Number(maybe_zero), "*", _)
                | (_, "*", MaybeLiteral::Number(maybe_zero))
                | (MaybeLiteral::Number(maybe_zero), "/", _) if maybe_zero == 0.0 =>
                {
                    Some(MaybeLiteral::Number(0.0).into())
                }
                (MaybeLiteral::Number(maybe_zero), "+", other)
                | (other, "+", MaybeLiteral::Number(maybe_zero))
                | (other, "-", MaybeLiteral::Number(maybe_zero)) => if maybe_zero == 0.0 {
                    Some(other.into())
                } else {
                    None
                },
                (MaybeLiteral::Number(maybe_one), "*", other)
                | (other, "*", MaybeLiteral::Number(maybe_one))
                | (other, "/", MaybeLiteral::Number(maybe_one)) => if maybe_one == 1.0 {
                    Some(other.into())
                } else {
                    None
                },
                (other, "**", MaybeLiteral::Number(exponent)) => if exponent == 0.0 {
                    Some(MaybeLiteral::Number(0.0).into())
                } else if exponent == 1.0 {
                    Some(other.into())
                } else {
                    None
                },

                _ => None,
            };
        }
    }
    if let Some(replacement) = should_replace_with {
        *expr = replacement;
    }
}

fn fold_constants_statement(statement: &mut Statement) {
    match statement {
        Statement::VariableAssignment { name: _, value } => fold_constants_expr(value),
        Statement::SinkAssignment { sink, value } => {
            if let Some(ref mut args) = sink.args {
                for arg in args.iter_mut() {
                    fold_constants_expr(arg);
                }
            }
            fold_constants_expr(value);
        }
        Statement::WhenBlock {
            condition,
            contents,
        } => {
            fold_constants_expr(condition);
            for statement in contents.iter_mut() {
                fold_constants_statement(statement);
            }
        }
    }
}

/// Replace trivial expressions like `1 + 1` with their results.
pub fn fold_constants<'a, 'b>(program: &'a Vec<Statement<'b>>) -> Vec<Statement<'b>> {
    let mut result = program.clone();
    for statement in result.iter_mut() {
        fold_constants_statement(statement);
    }
    result
}

fn get_variable_references<'a, 'b>(expression: &'b Expression<'a>) -> Vec<Token<'a>> {
    match expression {
        Expression::Variable(token) => vec![token.clone()],
        Expression::StringLiteral(components) => {
            let mut result = vec![];
            for component in components {
                match component {
                    StringLiteralComponent::StringData(_) => (),
                    StringLiteralComponent::Interpolation(interpolation) => {
                        result.append(&mut get_variable_references(&interpolation))
                    }
                }
            }
            result
        }
        Expression::FunctionEvaluation { name: _, args }
        | Expression::Source {
            component: _,
            source: _,
            args: Some(args),
        } => {
            let mut result = vec![];
            for arg in args {
                result.append(&mut get_variable_references(&arg));
            }
            result
        }
        Expression::WhenExpression { arms, otherwise } => {
            let mut result = get_variable_references(&*otherwise);
            for arm in arms {
                result.append(&mut get_variable_references(&arm.0));
                result.append(&mut get_variable_references(&arm.1));
            }
            result
        }
        Expression::Operation {
            lhs,
            operation: _,
            rhs,
        } => {
            let mut result = get_variable_references(&*lhs);
            result.append(&mut get_variable_references(&*rhs));
            result
        }
        Expression::Literal(_)
        | Expression::Undefined
        | Expression::Source {
            component: _,
            source: _,
            args: _,
        } => vec![],
    }
}

fn inline_variable<'a, 'b>(
    expression: &'b mut Expression<'a>,
    variable: &'b Token<'a>,
    value: &'b Expression<'a>,
) {
    if let Expression::Variable(name) = expression.clone() {
        if &name == variable {
            *expression = value.clone();
            return;
        }
    }
    match expression {
        Expression::StringLiteral(components) => for component in components {
            if let StringLiteralComponent::Interpolation(interpolation) = component {
                inline_variable(interpolation, variable, value);
            }
        },
        Expression::FunctionEvaluation { name: _, args }
        | Expression::Source {
            component: _,
            source: _,
            args: Some(args),
        } => for arg in args {
            inline_variable(arg, variable, value);
        },
        Expression::WhenExpression { arms, otherwise } => {
            for arm in arms {
                inline_variable(&mut arm.0, variable, value);
                inline_variable(&mut arm.1, variable, value);
            }
            inline_variable(otherwise, variable, value);
        }
        Expression::Operation {
            lhs,
            operation: _,
            rhs,
        } => {
            inline_variable(lhs, variable, value);
            inline_variable(rhs, variable, value);
        }
        Expression::Variable(_)
        | Expression::Literal(_)
        | Expression::Undefined
        | Expression::Source {
            component: _,
            source: _,
            args: _,
        } => (),
    }
}

/// Check that the given program contains no undefined variables or variable reference cycles. If a
/// variable is a constant, or only referenced in one place, inline it. Return the program sorted
/// topologically by variable use.
pub fn variable_pass<'a, 'b>(
    program: &'b Vec<Statement<'a>>,
) -> Result<Vec<Statement<'a>>, MDCLError<'a>> {
    #[derive(Clone, Debug, PartialEq, Eq, Hash)]
    enum GraphNode<'a> {
        Variable(Token<'a>),
        Sink(Sink<'a>),
    }
    let program_len = program.len();
    let mut graph = Graph::<GraphNode<'a>, u32>::new();
    let mut node_to_id = HashMap::new();
    let mut node_to_definition = HashMap::new();
    let mut potentially_undefined_vars = HashSet::new();
    let mut unused_union_find = UnionFind::new(program_len + 1);

    for statement in program {
        let definition = match statement {
            Statement::VariableAssignment { name, value } => {
                (GraphNode::Variable(name.clone()), value.clone())
            }
            Statement::SinkAssignment { sink, value } => {
                (GraphNode::Sink(sink.clone()), value.clone())
            }
            _ => {
                return Err(statement.make_error(
                    "Unsupported statement type for variable pass (run unblock_whens first!)",
                ))
            }
        };
        node_to_definition.insert(definition.0.clone(), definition.1.clone());
        if !node_to_id.contains_key(&definition.0) {
            node_to_id.insert(definition.0.clone(), graph.add_node(definition.0.clone()));
        };
        let definition_node_id = node_to_id.get(&definition.0).unwrap().clone();
        potentially_undefined_vars.remove(&definition.0);

        if definition_node_id.index() >= program_len {
            break; // There's definitely an undefined variable here.
        }

        if let GraphNode::Sink(_) = definition.0 {
            unused_union_find.union(definition_node_id.index(), program_len);
        }

        for reference in get_variable_references(&definition.1) {
            let reference = GraphNode::Variable(reference);
            if !node_to_id.contains_key(&reference) {
                node_to_id.insert(reference.clone(), graph.add_node(reference.clone()));
                potentially_undefined_vars.insert(reference.clone());
            }
            let reference_node_id = node_to_id.get(&reference).unwrap().clone();
            match graph.find_edge(reference_node_id, definition_node_id) {
                Some(index) => graph[index] += 1,
                None => {
                    graph.add_edge(reference_node_id, definition_node_id, 1);
                    if reference_node_id.index() < program_len {
                        unused_union_find
                            .union(definition_node_id.index(), reference_node_id.index());
                    }
                }
            }
        }
    }

    if !potentially_undefined_vars.is_empty() {
        let mut undefined_vars = vec![];
        for node in potentially_undefined_vars {
            if let GraphNode::Variable(name) = node {
                undefined_vars.push(name);
            }
        }
        return Err(MDCLError {
            description: "Undefined variable",
            bad_tokens: undefined_vars,
        });
    }

    let mut result = vec![];

    match toposort(&graph, None) {
        Ok(node_ids) => {
            for node_id in node_ids {
                let node = &graph[node_id];
                let mut value = node_to_definition[node].clone();
                fold_constants_expr(&mut value);
                match node {
                    GraphNode::Variable(name) => {
                        let is_unused = unused_union_find.find(node_id.index())
                            != unused_union_find.find(program_len);
                        let is_simple = match &value {
                            Expression::Undefined
                            | Expression::Literal(_)
                            | Expression::Variable(_) => true,
                            Expression::StringLiteral(components) => if let Some(
                                StringLiteralComponent::StringData(_),
                            ) = components.first()
                            {
                                components.len() == 1
                            } else {
                                false
                            },
                            _ => false,
                        };
                        let is_referenced_once = {
                            let mut total = 0;
                            for edge in graph.edges(node_id) {
                                total += edge.weight();
                            }
                            total <= 1
                        };
                        if is_unused {
                            // Don't include unused variables in the program.
                        } else if is_simple || is_referenced_once {
                            for edge in graph.edges(node_id) {
                                inline_variable(
                                    node_to_definition.get_mut(&graph[edge.target()]).unwrap(),
                                    &name,
                                    &value,
                                );
                            }
                        } else {
                            result.push(Statement::VariableAssignment {
                                name: name.clone(),
                                value,
                            });
                        }
                    }
                    GraphNode::Sink(sink) => {
                        result.push(Statement::SinkAssignment {
                            sink: sink.clone(),
                            value,
                        });
                    }
                }
            }
        }
        Err(cycle) => {
            use petgraph::algo::kosaraju_scc;
            let mut bad_tokens = match &graph[cycle.node_id()] {
                GraphNode::Variable(name) => vec![name.clone()],
                GraphNode::Sink(sink) => sink.get_tokens(),
            };
            for component in kosaraju_scc(&graph) {
                if component.len() <= 1 {
                    continue;
                }
                for nodeid in component {
                    match &graph[nodeid] {
                        GraphNode::Variable(name) => {
                            bad_tokens.push(name.clone());
                        }
                        _ => (),
                    }
                }
            }
            return Err(MDCLError {
                description: "Variable reference cycle(s)",
                bad_tokens,
            });
        }
    }
    Ok(result)
}

/// Split the given program into multiple sub-programs, with each sub-program corresponding to a
/// different sink component.
pub fn split_sinks<'a, 'b>(
    program: &'b Vec<Statement<'a>>,
) -> Result<HashMap<Option<Token<'a>>, Vec<Statement<'a>>>, MDCLError<'a>> {
    let mut variable_statements = vec![];
    let mut result: HashMap<_, Vec<Statement<'a>>> = HashMap::new();
    for statement in program {
        match statement {
            Statement::VariableAssignment { name: _, value: _ } => {
                variable_statements.push(statement.clone())
            }
            Statement::SinkAssignment { sink, value } => {
                let key = sink.component.clone();
                let new_statement = Statement::SinkAssignment {
                    sink: Sink {
                        component: None,
                        sink: sink.sink.clone(),
                        args: sink.args.clone(),
                    },
                    value: value.clone(),
                };
                if result.contains_key(&key) {
                    result.get_mut(&key).unwrap().push(new_statement);
                } else {
                    result.insert(key, vec![new_statement]);
                }
            }
            _ => return Err(statement.make_error("Unsupported statement type in split-sinks pass")),
        }
    }
    for subprogram in result.values_mut() {
        subprogram.append(&mut variable_statements.clone());
    }
    Ok(result)
}

fn rename_variables<'a, 'b>(
    expression: &'b mut Expression<'a>,
    renames: &'b HashMap<Token<'a>, Token<'a>>,
) {
    match expression {
        Expression::StringLiteral(components) => for component in components {
            match component {
                StringLiteralComponent::Interpolation(expr) => rename_variables(expr, renames),
                StringLiteralComponent::StringData(_) => (),
            }
        },
        Expression::Literal(_)
        | Expression::Undefined
        | Expression::Source {
            component: _,
            source: _,
            args: None,
        } => (),
        Expression::FunctionEvaluation { name: _, args }
        | Expression::Source {
            component: _,
            source: _,
            args: Some(args),
        } => for arg in args {
            rename_variables(arg, renames);
        },
        Expression::WhenExpression { arms, otherwise } => {
            rename_variables(otherwise, renames);
            for arm in arms {
                rename_variables(&mut arm.0, renames);
                rename_variables(&mut arm.1, renames);
            }
        }
        Expression::Operation {
            lhs,
            operation: _,
            rhs,
        } => {
            rename_variables(lhs, renames);
            rename_variables(rhs, renames);
        }
        Expression::Variable(name) => match renames.get(&name) {
            Some(new_name) => *name = new_name.clone(),
            None => (),
        },
    }
}

const NAME_CHARS: &'static [u8] = b"_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

pub fn minify_variable_names<'a, 'b>(
    program: &'b Vec<Statement<'a>>,
) -> Result<Vec<Statement<'a>>, MDCLError<'a>> {
    let mut result = vec![];
    let mut rename_table = HashMap::new();
    for statement in program {
        match statement {
            Statement::VariableAssignment { name, value } => {
                let mut new_name = String::new();
                let mut name_id = rename_table.len() + 1;
                while name_id > 0 {
                    new_name.push(NAME_CHARS[name_id % NAME_CHARS.len()] as char);
                    name_id /= NAME_CHARS.len();
                }
                let new_name = Token::Identifier(Cow::from(new_name));
                rename_table.insert(name.clone(), new_name.clone());
                result.push(Statement::VariableAssignment {
                    name: new_name,
                    value: value.clone(),
                })
            }
            _ => result.push(statement.clone()),
        }
    }
    for statement in &mut result {
        match statement {
            Statement::VariableAssignment { name: _, value }
            | Statement::SinkAssignment {
                sink:
                    Sink {
                        component: _,
                        sink: _,
                        args: None,
                    },
                value,
            } => rename_variables(value, &rename_table),
            Statement::SinkAssignment {
                sink:
                    Sink {
                        component: _,
                        sink: _,
                        args: Some(args),
                    },
                value,
            } => {
                rename_variables(value, &rename_table);
                for arg in args {
                    rename_variables(arg, &rename_table);
                }
            }
            _ => {
                return Err(statement
                    .make_error("Unsupported statement type in variable minification pass"))
            }
        }
    }
    Ok(result)
}
