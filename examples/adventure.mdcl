# Adventure game sample program

## State variables
# Defines a macro `state_variable [name, initial_value]` that creates current_[name] and next_[name] variables.
# When the "Continue" button is pressed, the next_[name] value will become the current_[name] value.
resetting = false
macro [state_variable [name, initial]
    concat_idents [current_ name] = when button.pressCount > 0 button.lastValue(stringify [name]) otherwise initial
    concat_idents [next_ name] = when resetting initial otherwise concat_idents [current_ name]
    button.capture(stringify [name]): concat_idents [next_ name]
]

## Pages and choices
# See "Game" section for macro usage examples
state_variable [page, page_home]
macro [page [name]
    concat_idents [page_ name] = counter [page]
    when current_page = concat_idents [page_ name]
]

choices.choiceContent(1): ""
choices.choiceContent(2): ""
choices.choiceContent(3): ""
button.hidden: true
last_selected_button = when button.pressCount > 0 button.lastValue("last_selected_button") otherwise 999
button.capture("last_selected_button"): selected_button
selected_button =
    when choices.isSelected(1) 1
    when choices.isSelected(2) 2
    when choices.isSelected(3) 3
    otherwise 999
selection = when selected_button < last_selected_button selected_button when selected_button > last_selected_button selected_button - 1 otherwise -1
macro [choice [choice_number, description]
    when last_selected_button <= choice_number {
        choices.choiceContent(choice_number + 1): description
    }
    when last_selected_button > choice_number {
        choices.choiceContent(choice_number): description
    }
    when selection = choice_number {
        button.hidden: false
    }

    when selection = choice_number
]

## Game
description.content: ""

page [home] {
    description.content: "Welcome home!"
    choice [1, "Go to work"] {
        next_page = page_work
    }
    choice [2, "Go to school"] {
        next_page = page_school
    }
}

page [work] {
    description.content: "You're at work now."
    choice [1, "Go back home"] {
        next_page = page_home
    }
}

page [school] {
    description.content: "You're at school."
    choice [1, "Go back home"] {
        next_page = page_home
    }
    choice [2, "Secret page"] {
        next_page = page_secret
    }
}

page [secret] {
    description.content: "It's a secret to everyone!"
    choice [1, "Back to school"] {
        next_page = page_school
    }
    choice [2, "Reset the game"] {
        resetting = true
    }
}
