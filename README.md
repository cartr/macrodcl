# MacroDCL

MacroDCL extends the [Desmos Computation Layer][dcl] language with features
that make it easier to write complicated programs. It is implemented in Rust
as a compiler that takes MacroDCL programs and produces minified DCL code.

[dcl]: https://teacher.desmos.com/computation-layer/documentation

## Project Status

I wrote most of MacroDCL on a train ride. The compiler is probably kinda buggy,
the error messages aren't very helpful, and parts of the language weren't
thought out very well. Call it an "early prototype".

The language syntax, command-line interface, and crate API could change at any
time without notice, so please don't rely on MacroDCL for anything important.

## Features

### Arithmetic operators

MacroDCL allows you to use `+`, `-`, `*`, `/`, and `**` in your code:

```mdcl
position = initial_position - gravity * t**2 / 2
```

I'm not really sure why Desmos didn't include these in their language; it seems
like an obvious feature to have, especially for a graphing calculator company.

### `when`-blocks

Normally, the only conditional operator in DCL is `when`, which behaves
similarly to the ternary operator in C:

```dcl
content:
    when answer > 5 "Too high!"
    when answer < 5 "Too low!"
    otherwise "Correct!"
```

MacroDCL adds a block syntax for `when`, so you can change multiple
variables and sinks with the same condition:

```mdcl
content: ""
correct: false
when answer = 5 {
    content: "Correct!"
    correct: true
}

# Equivalent to the following DCL code:
content: when answer = 5 "Correct" otherwise ""
correct: answer = 5
```

### Single-file programs

MacroDCL lets you specify multiple components' sinks in the same source file:

```mdcl
note.content: "Content goes here!"
choices.correct: choices.isSelected(2)
```

The compiler will split the program up into sections for each component that
you can copy-paste into the Activity Builder.

### Macros

True to its name, MacroDCL includes a compile-time macro system. Macros are
invoked using square brackets, like `macro_name [args]`. The following built-in
macros are available for you to use:

#### `nop [tokens]`

Does nothing, returning the input tokens unmodified.

#### `macro [macro_name [arg1, arg2, ...] macro_result]`

Define your very own custom macro! Example:

```mdcl
macro [set_to_zero [var] var = 0]
set_to_zero [a]
set_to_zero [b]

# Equivalent to:
a = 0
b = 0
```

#### `counter [name]`

Increment the counter with the given name and paste its value into the
program. Example:

```mdcl
a = counter [some_counter_name]
b = counter [some_counter_name]

# Equivalent to:
a = 0
b = 1
```

#### `concat_idents [ident1 ident2 ...]`

Concatenate the given identifiers into one big identifier.
`concat_idents [a b c]` is `abc`.

#### `stringify [tokens]`

Concatenate the given tokens and place them into a string literal.
`stringify [a b c]` is `"abc"`.

#### `set_macro_limit [limit]`

Set the number of times you can invoke a macro before the compiler throws an
error and quits. (We impose a limit to prevent you from blowing up the compiler
with an infinitely-recursive macro.)

### Optimizations

The MacroDCL compiler will try to make your program shorter by inlining and
renaming variables, simplifying constant expressions, and removing unreached
code.

## Unsupported DCL features

While we strive to support most of DCL's features in MacroDCL, there are a
couple of things you can't use yet:

### `component.script.var`

DCL allows you to access variables from other components by writing
`[component name].script.[variable name]`. MacroDCL doesn't support this
syntax because in MacroDCL, you put all your variable and sink assignments into
one big program and the compiler splits it into components for you.

### Type attributes

In DCL, certain esoteric object types include properties you can access with
dot-notation. The only problem is that dot-notation is also used by sources:

```dcl
# Using a type attribute:
myPair = parseOrderedPair(input.latex)
number("a"): myPair.x

# Using a source (note identical syntax):
number("b"): input.pressCount
```

It would be kind of hard to separate these two cases, so currently MacroDCL
doesn't try to handle type attributes.

## JavaScript compiler

MacroDCL also includes `macrodcl-js`, a program that compiles MacroDCL to JavaScript.

The generated code contains a function called `update_mdcl_state`, which you should call whenever one of your sources changes. You'll also need to provide:

- A JavaScript function called `func$[function name]` for every function your MacroDCL code calls. For example, `function func$not(val) { return !val; }`.
- A JavaScript function called `source$[component]$[source name]` for every source referenced in your MacroDCL code.
- A JavaScript function called `sink$[component]$[sink name]` for every sink in your MacroDCL code.

You'll probably also want to run the generated code through a program like [Closure Compiler](https://developers.google.com/closure/compiler/).
